package by.vsu.epam.project.controller.account;

import by.vsu.epam.project.controller.BaseController;
import by.vsu.epam.project.domain.Account;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.ServiceFactory;
import by.vsu.epam.project.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AccountListController extends BaseController {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServiceFactory factory = getServiceFactory();
		AccountService accountService = factory.newAccountServiceInstance();
		try {
			List<Account> accounts = accountService.findAll();
			req.setAttribute("accounts", accounts);
			req.getRequestDispatcher("/WEB-INF/jsp/account/list.jsp").forward(req, resp);
		} catch(ServiceException e) {
			throw new ServletException(e);
		}
	}
}
