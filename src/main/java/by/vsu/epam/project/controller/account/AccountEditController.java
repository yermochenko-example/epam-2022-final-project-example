package by.vsu.epam.project.controller.account;

import by.vsu.epam.project.controller.BaseController;
import by.vsu.epam.project.domain.Account;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.ServiceFactory;
import by.vsu.epam.project.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccountEditController extends BaseController {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String idStr = req.getParameter("id");
		if(idStr != null) {
			try {
				Long id = Long.valueOf(idStr);
				ServiceFactory factory = getServiceFactory();
				AccountService accountService = factory.newAccountServiceInstance();
				try {
					Account account = accountService.findById(id);
					if(account != null) {
						req.setAttribute("account", account);
					} else {
						throw new IllegalArgumentException("Account not exists");
					}
				} catch(ServiceException e) {
					throw new ServletException(e);
				}
			} catch(IllegalArgumentException e) {
				resp.sendError(404);
				return;
			}
		}
		req.getRequestDispatcher("/WEB-INF/jsp/account/edit.jsp").forward(req, resp);
	}
}
