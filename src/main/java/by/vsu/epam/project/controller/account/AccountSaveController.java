package by.vsu.epam.project.controller.account;

import by.vsu.epam.project.controller.BaseController;
import by.vsu.epam.project.domain.Account;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.ServiceFactory;
import by.vsu.epam.project.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccountSaveController extends BaseController {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String id = req.getParameter("id");
		String client = req.getParameter("client");
		if(client != null && !client.isBlank()) {
			Account account = new Account();
			account.setClient(client);
			if(id != null) {
				try {
					account.setId(Long.valueOf(id));
				} catch(NumberFormatException e) {
					resp.sendError(400);
					return;
				}
			}
			ServiceFactory factory = getServiceFactory();
			AccountService accountService = factory.newAccountServiceInstance();
			try {
				accountService.save(account);
				resp.sendRedirect(req.getContextPath() + "/account/list.html");
			} catch(ServiceException e) {
				throw new ServletException(e);
			}
		} else {
			resp.sendError(400);
		}
	}
}
