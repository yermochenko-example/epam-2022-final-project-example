package by.vsu.epam.project.controller;

import by.vsu.epam.project.service.ServiceFactory;
import by.vsu.epam.project.service.logic.ServiceFactoryTestImpl;

import javax.servlet.http.HttpServlet;

abstract public class BaseController extends HttpServlet {
	public ServiceFactory getServiceFactory() {
		return new ServiceFactoryTestImpl();
	}
}
