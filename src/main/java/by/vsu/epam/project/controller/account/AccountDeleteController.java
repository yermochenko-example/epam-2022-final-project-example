package by.vsu.epam.project.controller.account;

import by.vsu.epam.project.controller.BaseController;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.ServiceFactory;
import by.vsu.epam.project.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccountDeleteController extends BaseController {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		ServiceFactory factory = getServiceFactory();
		AccountService accountService = factory.newAccountServiceInstance();
		try {
			try {
				accountService.delete(Long.valueOf(id));
			} catch(NumberFormatException e) {}
			resp.sendRedirect(req.getContextPath() + "/account/list.html");
		} catch(ServiceException e) {
			throw new ServletException(e);
		}
	}
}
