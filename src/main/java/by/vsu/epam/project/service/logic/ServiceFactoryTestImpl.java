package by.vsu.epam.project.service.logic;

import by.vsu.epam.project.dao.AccountDao;
import by.vsu.epam.project.dao.test.AccountTestDaoImpl;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.ServiceFactory;

public class ServiceFactoryTestImpl implements ServiceFactory {
	private AccountDao accountDao;

	private AccountDao getAccountDao() {
		if(accountDao == null) {
			accountDao = new AccountTestDaoImpl();
		}
		return accountDao;
	}

	@Override
	public AccountService newAccountServiceInstance() {
		AccountServiceImpl accountService = new AccountServiceImpl();
		accountService.setAccountDao(getAccountDao());
		return accountService;
	}
}
