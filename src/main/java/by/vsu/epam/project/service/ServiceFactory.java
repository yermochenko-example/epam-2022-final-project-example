package by.vsu.epam.project.service;

public interface ServiceFactory {
	AccountService newAccountServiceInstance();
}
