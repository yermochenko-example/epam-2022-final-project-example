package by.vsu.epam.project.service.logic;

import by.vsu.epam.project.dao.AccountDao;
import by.vsu.epam.project.dao.DaoException;
import by.vsu.epam.project.domain.Account;
import by.vsu.epam.project.service.AccountService;
import by.vsu.epam.project.service.exception.ServiceException;

import java.util.List;

public class AccountServiceImpl implements AccountService {
	private AccountDao accountDao;

	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@Override
	public Account findById(Long id) throws ServiceException {
		try {
			return accountDao.read(id);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Account> findAll() throws ServiceException {
		try {
			return accountDao.readAll();
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void save(Account account) throws ServiceException {
		try {
			if(account.getId() != null) {
				Account old = accountDao.read(account.getId());
				account.setBalance(old.getBalance());
				accountDao.update(account);
			} else {
				account.setBalance(0L);
				accountDao.create(account);
			}
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			accountDao.delete(id);
		} catch(DaoException e) {
			throw new ServiceException(e);
		}
	}
}
