package by.vsu.epam.project.service;

import by.vsu.epam.project.domain.Account;
import by.vsu.epam.project.service.exception.ServiceException;

import java.util.List;

public interface AccountService {
	Account findById(Long id) throws ServiceException;

	List<Account> findAll() throws ServiceException;

	void save(Account account) throws ServiceException;

	void delete(Long id) throws ServiceException;
}
