package by.vsu.epam.project.service.exception;

public class ServiceException extends Exception {
	public ServiceException(Throwable cause) {
		super(cause);
	}
}
