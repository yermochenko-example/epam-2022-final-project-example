package by.vsu.epam.project.domain;

public class Account extends Entity {
	private String client;
	private Long balance;

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}
}
