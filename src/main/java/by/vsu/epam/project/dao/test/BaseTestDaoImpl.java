package by.vsu.epam.project.dao.test;

import by.vsu.epam.project.dao.Dao;
import by.vsu.epam.project.domain.Entity;

import java.util.Map;

abstract public class BaseTestDaoImpl<T extends Entity> implements Dao<T> {
	private final Map<Long, T> entities;

	protected BaseTestDaoImpl(Map<Long, T> entities) {
		this.entities = entities;
	}

	protected Map<Long, T> getEntities() {
		return entities;
	}

	@Override
	public Long create(T entity) {
		Long id = 0L;
		if(!entities.isEmpty()) {
			id = entities.keySet().stream().max(Long::compare).get();
		}
		id++;
		entity.setId(id);
		entities.put(id, entity);
		return id;
	}

	@Override
	public T read(Long id) {
		return entities.get(id);
	}

	@Override
	public void update(T entity) {
		if(entities.containsKey(entity.getId())) {
			entities.put(entity.getId(), entity);
		}
	}

	@Override
	public void delete(Long id) {
		entities.remove(id);
	}
}
