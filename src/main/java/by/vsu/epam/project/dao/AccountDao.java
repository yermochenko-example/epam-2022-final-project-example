package by.vsu.epam.project.dao;

import by.vsu.epam.project.domain.Account;

import java.util.List;

public interface AccountDao extends Dao<Account> {
	List<Account> readAll() throws DaoException;
}
