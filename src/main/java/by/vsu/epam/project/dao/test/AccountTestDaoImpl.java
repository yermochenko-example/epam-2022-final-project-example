package by.vsu.epam.project.dao.test;

import by.vsu.epam.project.dao.AccountDao;
import by.vsu.epam.project.domain.Account;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AccountTestDaoImpl extends BaseTestDaoImpl<Account> implements AccountDao {
	private static final Map<Long, Account> accounts = new ConcurrentHashMap<>();
	static {
		Account account;
		account = new Account();
		account.setId(1L);
		account.setClient("Иванов");
		account.setBalance(123_45L);
		accounts.put(account.getId(), account);
		account = new Account();
		account.setId(2L);
		account.setClient("Петров");
		account.setBalance(345_67L);
		accounts.put(account.getId(), account);
		account = new Account();
		account.setId(3L);
		account.setClient("Сидоров");
		account.setBalance(456_78L);
		accounts.put(account.getId(), account);
		account = new Account();
		account.setId(4L);
		account.setClient("Васильев");
		account.setBalance(678_90L);
		accounts.put(account.getId(), account);
	}

	public AccountTestDaoImpl() {
		super(accounts);
	}

	@Override
	public List<Account> readAll() {
		return getEntities().values().stream().toList();
	}
}
