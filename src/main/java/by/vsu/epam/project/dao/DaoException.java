package by.vsu.epam.project.dao;

public class DaoException extends Exception {
	public DaoException(Throwable cause) {
		super(cause);
	}
}
