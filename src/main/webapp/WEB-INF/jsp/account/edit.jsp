<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="account" scope="request" class="by.vsu.epam.project.domain.Account" />
<c:choose>
	<c:when test="${not empty account.id}">
		<fmt:formatNumber var="accountId" pattern="0000" value="${account.id}" />
		<c:set var="title" value="Редактирование счёта [${accountId}]" />
	</c:when>
	<c:otherwise>
		<c:set var="title" value="Добавление нового счёта" />
	</c:otherwise>
</c:choose>
<u:html title="${title}">
	<c:url var="saveUrl" value="/account/save.html" />
	<form action="${saveUrl}" method="post">
		<c:if test="${not empty account.id}">
			<input type="hidden" name="id" value="${account.id}">
		</c:if>
		<label for="client-input">Имя клиента:</label><br>
		<input id="client-input" type="text" name="client" value="${account.client}">
		<c:if test="${not empty account.id}">
			<p>Текущий баланс: <strong><u:currency value="${account.balance}"/></strong></p>
		</c:if>
		<button type="submit">Сохранить</button>
	</form>
	<c:if test="${not empty account.id}">
		<c:url var="deleteUrl" value="/account/delete.html" />
		<form action="${deleteUrl}" method="post">
			<input type="hidden" name="id" value="${account.id}">
			<button type="submit">Удалить</button>
		</form>
	</c:if>
</u:html>