<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="accounts" scope="request" type="java.util.List"/>

<u:html title="Список счетов">
	<ul>
		<c:forEach var="account" items="${accounts}">
			<li>
				<c:url var="editUrl" value="/account/edit.html">
					<c:param name="id" value="${account.id}"/>
				</c:url>
				[<a href="${editUrl}"><fmt:formatNumber value="${account.id}" pattern="0000"/></a>]
					${account.client},
				<u:currency value="${account.balance}"/>
			</li>
		</c:forEach>
		<c:url var="editUrl" value="/account/edit.html" />
		<p><a href="${editUrl}">Добавить</a></p>
	</ul>
</u:html>