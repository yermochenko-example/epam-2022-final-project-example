<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="title" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Банк «Рога и копыта» :: ${title}</title>
	<c:url var="mainCssUrl" value="/main.css" />
	<link rel="stylesheet" href="${mainCssUrl}" type="text/css">
</head>
<body>
<h1>${title}</h1>
<jsp:doBody/>
<div style="color: white; background: darkblue;">&copy;&nbsp;ВГУ имени П. М. Машерова</div>
</body>
</html>