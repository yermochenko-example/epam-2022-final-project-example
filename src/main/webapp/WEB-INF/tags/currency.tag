<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="value" required="true" rtexprvalue="true" type="java.lang.Long"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<span style="color: red;">
<fmt:formatNumber value="${(value - (value mod 100)) div 100}" pattern="#"/> руб.,
<fmt:formatNumber value="${value mod 100}" pattern="00"/> коп.
</span>